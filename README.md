# Distributed Systems Course

[Scenes from Distributed Systems](https://drawings.jvns.ca/drawings/distributed-systems.svg)

[План лекций](https://docs.google.com/document/d/1x7Z7DRiHuZ_krkW_oW-r7-IGSF-FdAzxY7KCh7CaDpE/edit?usp=sharing)

## Добро пожаловать!

[Настраиваем рабочее окружение](docs/setup.md)

[Как сдавать задачи](docs/ci.md)

[Приложение](https://en.wikipedia.org/wiki/HTTP_404) – выключено

[Задачи](tasks/all-tasks)

## Библиотеки

* [Wheels](https://gitlab.com/Lipovsky/wheels) – Базовые компоненты
* [Await](https://gitlab.com/Lipovsky/await) – Файберы, фьючи и комбинаторы
* [Whirl](https://gitlab.com/Lipovsky/whirl) – Детерминированный симулятор распределенной системы
