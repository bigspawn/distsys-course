#pragma once

#include <rsm/kv/store/store.hpp>

#include <rsm/raft/state_machine.hpp>

#include <map>

namespace kv {

// State machine implementation for Raft RSM

::raft::IStateMachinePtr MakeKVStateMachine();

}  // namespace kv
