#pragma once

#include <rsm/raft/client.hpp>
#include <rsm/kv/client/client.hpp>

#include <whirl/matrix/client/client.hpp>

namespace whirl {

class KVClientBase : public ClientBase {
 public:
  KVClientBase(NodeServices runtime) : ClientBase(std::move(runtime)) {
  }

 protected:
  kv::BlockingClient MakeKVClient() {
    return kv::BlockingClient(raft::BlockingClient(
        Cluster(), RPCClient(), UidsGenerator(), TimeService(), NodeLogger()));
  }
};

}  // namespace whirl
