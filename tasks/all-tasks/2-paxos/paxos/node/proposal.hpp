#pragma once

#include <consensus/value.hpp>

#include <whirl/cereal/serialize.hpp>

namespace paxos {

////////////////////////////////////////////////////////////////////////////////

using Value = consensus::Value;

////////////////////////////////////////////////////////////////////////////////

// Proposal number

////////////////////////////////////////////////////////////////////////////////

// Proposal = Proposal number + Value

}  // namespace paxos
