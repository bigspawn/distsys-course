#include <rsm/multipaxos/command.hpp>

namespace multipaxos {

std::ostream& operator<<(std::ostream& out, const Command& command) {
  out << command.name << "/" << command.id;
  return out;
}

Command MakeNopCommand() {
  return Command{"Nop-Id", "Nop", CommandInput{}};
}

}  // namespace multipaxos
