#include <rsm/multipaxos/multipaxos.hpp>

#include <rsm/multipaxos/log.hpp>

#include <whirl/node/peer_base.hpp>
#include <whirl/node/logging.hpp>

// Concurrency
#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>
#include <await/fibers/sync/channel.hpp>
#include <await/fibers/sync/mutex.hpp>

namespace multipaxos {

using whirl::PeerBase;

using await::fibers::Await;
using await::fibers::Channel;
using await::futures::Promise;

////////////////////////////////////////////////////////////////////////////////

class MultiPaxosRSM : public IReplicatedStateMachine,
                      public PeerBase,
                      public std::enable_shared_from_this<MultiPaxosRSM> {
 public:
  MultiPaxosRSM(std::string name, IStateMachinePtr state_machine,
                NodeServices runtime)
      : PeerBase(std::move(runtime)),
        name_(std::move(name)),
        state_machine_(std::move(state_machine)) {
  }

  Future<CommandOutput> Execute(Command command) override {
    auto [future, promise] = await::futures::MakeContract<CommandOutput>();

    // TODO: submit command to Multi-Paxos pipeline
    {
      // Immediately apply command to local state machine
      // This is just totally wrong

      std::lock_guard guard(mutex_);

      auto result = state_machine_->Apply(command);
      // Complete pending Execute in kv::ClientService
      std::move(promise).Set(result);
    }

    return std::move(future);
  };

  void Start() override {
    // TODO: Register RPC services, spawn pipeline threads...
  }

 private:
  std::string name_;

  // Replicated state
  IStateMachinePtr state_machine_;
  await::fibers::Mutex mutex_;
};

////////////////////////////////////////////////////////////////////////////////

IReplicatedStateMachinePtr MakeMultiPaxosRSM(const std::string& name,
                                             IStateMachinePtr state_machine,
                                             NodeServices runtime) {
  return std::make_shared<MultiPaxosRSM>(name, std::move(state_machine),
                                         std::move(runtime));
}

}  // namespace multipaxos
