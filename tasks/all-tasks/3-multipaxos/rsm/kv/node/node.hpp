#pragma once

#include <rsm/multipaxos/multipaxos.hpp>

#include <rsm/kv/node/state_machine.hpp>
#include <rsm/kv/node/client_service.hpp>

#include <whirl/node/node_base.hpp>

namespace kv {

using whirl::NodeBase;
using whirl::NodeServices;
using whirl::rpc::IServerPtr;

////////////////////////////////////////////////////////////////////////////////

class Node : public NodeBase {
 public:
  Node(NodeServices runtime) : NodeBase(runtime) {
  }

 protected:
  void RegisterRPCServices(const IServerPtr& rpc_server) override {
    // State machine
    auto kv_state_machine = std::make_shared<StateMachine>();

    // Replicated state machine via Multi-Paxos
    auto kv_rsm = MakeMultiPaxosRSM("kv", kv_state_machine, ThisNodeServices());
    kv_rsm->Start();

    // Client RPC service
    rpc_server->RegisterService(
        "KV", std::make_shared<ClientService>(ThisNodeServices(), kv_rsm));
  }

  void MainThread() override {
    // Do nothing
  }
};

}  // namespace kv
