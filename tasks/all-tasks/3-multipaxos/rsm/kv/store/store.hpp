#pragma once

#include <rsm/kv/store/types.hpp>

#include <map>

namespace kv {

// Replicated object: in-memory K->V mapping

class TinyKVStore {
 public:
  void Set(Key key, Value value) {
    map_.insert_or_assign(key, value);
  }

  Value Get(Key key) const {
    if (auto it = map_.find(key); it != map_.end()) {
      return it->second;
    } else {
      return ValueTraits::Default();
    }
  }

  Value Cas(Key key, Value expected_value, Value target_value) {
    auto old_value = Get(key);
    if (old_value == expected_value) {
      Set(key, target_value);
    }
    return old_value;
  }

  // Support iteration over K/V pairs

  auto begin() const {
    return map_.begin();
  }

  auto end() const {
    return map_.end();
  }

 private:
  std::map<Key, Value> map_;
};

}  // namespace kv
